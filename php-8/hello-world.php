<?php
  if (version_compare(phpversion(), '8.0.0', '>=') && version_compare(phpversion(), '9.0.0', '<')) {
    echo 'Hello, World! I am in php8 (v' . PHP_VERSION . " detected)\n";
  } else {
      throw new Exception('I am not in php8 (v' . PHP_VERSION . ' detected)');
  }
